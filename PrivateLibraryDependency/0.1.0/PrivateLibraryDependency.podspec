#
# Be sure to run `pod lib lint PrivateLibraryDependency.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PrivateLibraryDependency'
  s.version          = '0.1.0'
  s.summary          = 'PrivateLibraryDependency for test'

  s.description      = <<-DESC
PrivateLibraryDependency for dependency test.
                       DESC

  s.homepage         = 'https://gitlab.com/zlanchun/PrivateLibraryDependency'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'feimengchang' => 'zlanchun@icloud.com' }
  s.source           = { :git => 'https://zlanchun@gitlab.com/zlanchun/PrivateLibraryDependency.git', :tag => s.version.to_s }

  s.ios.deployment_target = '7.0'
  s.platform     = :ios, '7.0'
  s.source_files = 'PrivateLibraryDependency/Classes/**/*'

  s.dependency 'SDWebImage', '~> 4.0.0'
  #私库依赖
  s.dependency 'PrivateLibraryRepo', '~> 0.1.0'
end
